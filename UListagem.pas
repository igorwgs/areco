unit UListagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtCtrls,
  UDMRegraNegocio, Vcl.Buttons;

type
  TFMListProdutos = class(TForm)
    Panel1: TPanel;
    Label5: TLabel;
    DBGrid1: TDBGrid;
    SpeedButton2: TSpeedButton;
    Panel2: TPanel;
    SpeedButton3: TSpeedButton;
    SpeedButton1: TSpeedButton;
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    procedure AbrirCadastro;
    procedure AdicionarProduto;
    procedure RemoverProduto;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMListProdutos: TFMListProdutos;

implementation

uses UProduto;

{$R *.dfm}

procedure TFMListProdutos.DBGrid1DblClick(Sender: TObject);
begin
     AbrirCadastro;
end;

procedure TFMListProdutos.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key = VK_RETURN then
     begin
          AbrirCadastro;
     end;
end;

procedure TFMListProdutos.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if Key = 107 then
     begin
          AdicionarProduto;
     end
     else
     begin
          if Key = 109 then
          begin
               RemoverProduto;
          end;
     end;

end;

procedure TFMListProdutos.SpeedButton1Click(Sender: TObject);
begin
     AdicionarProduto;
end;

procedure TFMListProdutos.SpeedButton2Click(Sender: TObject);
var
   vMsg: String;
begin
     vMsg := 'Teclas de atalho:' + sLineBreak + sLineBreak +
             '-> Clique em Adicionar ou tecle "+" para cadastrar produto.' + sLineBreak +
             '-> Clique em Apagar ou tecle "-" para remover o produto selecionado.' + sLineBreak +
             '-> D� duplo-clique ou tecle enter para editar o produto selecionado.';

     Application.MessageBox(PWideChar(vMsg), PWideChar('Ajuda'), MB_ICONQUESTION);
end;

procedure TFMListProdutos.SpeedButton3Click(Sender: TObject);
begin
     RemoverProduto;
end;

procedure TFMListProdutos.AbrirCadastro;
var
   vFMProduto: TFMProduto;
begin
     vFMProduto := TFMProduto.Create(Self);
     try
        //Atualiza a tabela de cadastro antes de abrir a tela, pois pode ter havido altera��es
        DMRegraNegocio.CarregaTabela(DMRegraNegocio.FDProduto);

        DMRegraNegocio.FDProduto.Locate('codigo', DMRegraNegocio.FDListProduto.FieldByName('codigo').AsInteger,[]);

        vFMProduto.ShowModal;

        //Carrega as altera��es feito no cadastro de produto
        DMRegraNegocio.CarregaTabela(DMRegraNegocio.FDListProduto);
     finally
            FreeAndNil(vFMProduto);
     end;
end;

procedure TFMListProdutos.AdicionarProduto;
var
  vFMProduto: TFMProduto;
begin
     vFMProduto := TFMProduto.Create(Self);
     try
         //Atualiza a tabela de cadastro antes de abrir a tela, pois pode ter havido altera��es
         DMRegraNegocio.CarregaTabela(DMRegraNegocio.FDProduto);
         DMRegraNegocio.FDProduto.Append;
         vFMProduto.ShowModal;

         //Carrega as altera��es feito no cadastro de produto
         DMRegraNegocio.CarregaTabela(DMRegraNegocio.FDListProduto);
     finally
            FreeAndNil(vFMProduto);
     end;
end;

procedure TFMListProdutos.RemoverProduto;
begin
     if DMRegraNegocio.FDListProduto.RecordCount > 0 then
     begin
          if Application.MessageBox(PWideChar('Deseja apagar o registro selecionado?'),
                                    PWideChar('Apagar registro'),
                                    MB_YESNO + MB_ICONQUESTION) = mrYes then
          begin
               DMRegraNegocio.RemoverProduto;
          end;
     end;
end;

end.
