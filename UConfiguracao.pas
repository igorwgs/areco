unit UConfiguracao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFMConfiguracao = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Label1: TLabel;
    edBanco: TEdit;
    Label2: TLabel;
    edUsuario: TEdit;
    Label3: TLabel;
    edSenha: TEdit;
    Label4: TLabel;
    edServidor: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses UDMRegraNegocio;

{$R *.dfm}

procedure TFMConfiguracao.BitBtn1Click(Sender: TObject);
var
   vRetorno: TRetorno;
begin
     vRetorno := DMRegraNegocio.Conectar(edServidor.Text,
                                         edBanco.Text,
                                         edUsuario.Text,
                                         edSenha.Text);

     if vRetorno.Result then
     begin
          //Se conseguir conectar fecha a tela
          Self.Close;
     end
     else
     begin
          Application.MessageBox(PWideChar(vRetorno.Msg), PWideChar('Falha ao conectar'), MB_ICONWARNING);
     end;
end;

end.
