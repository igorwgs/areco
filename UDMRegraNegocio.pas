unit UDMRegraNegocio;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL, Data.DB,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.VCLUI.Wait, FireDAC.Phys.ODBCBase, FireDAC.Comp.UI;

type
  TRetorno = record
    Result: Boolean;
    Msg: String;
  end;

  TDMRegraNegocio = class(TDataModule)
    Connection: TFDConnection;
    FDListProduto: TFDQuery;
    DSListProduto: TDataSource;
    FDProduto: TFDTable;
    DSProduto: TDataSource;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink;
    FDQueryAux: TFDQuery;
    procedure FDProdutoAfterPost(DataSet: TDataSet);
    procedure FDProdutoAfterDelete(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure FDProdutoBeforePost(DataSet: TDataSet);
    procedure FDProdutoNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    function Conectar(pServidor, pBanco, pUsuario, pSenha: String): TRetorno;
    function CarregaTabela(pFDDataset: TFDCustomQuery): TRetorno;
    function RemoverProduto: TRetorno;
    { Public declarations }
  end;

var
  DMRegraNegocio: TDMRegraNegocio;

implementation

uses UConfiguracao;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDataModule2 }

function TDMRegraNegocio.CarregaTabela(pFDDataset: TFDCustomQuery): TRetorno;
begin
     try
        //Disabilidade controles de tela para ter bom desempenho, e n�o ficar piscando registros na tela do usuario
        pFDDataset.DisableControls;
        try
           pFDDataset.Close;
           pFDDataset.Open;

           Result.Result := True;
        finally
               pFDDataset.EnableControls;
        end;
     except
        on e: exception do
        begin
             Result.Result := False;
             Result.Msg := 'N�o foi poss�vel recarregar os produtos.'+
                            sLineBreak + sLineBreak + e.Message;
        end;
     end;
end;

function TDMRegraNegocio.Conectar(pServidor, pBanco, pUsuario, pSenha: String): TRetorno;
begin
     Connection.Params.Clear;
     Connection.Params.Add('DriverID=MSSQL');
     Connection.Params.Add('Database='+ pBanco);
     Connection.Params.Add('User_Name='+ pUsuario);
     Connection.Params.Add('Password='+ pSenha);
     Connection.Params.Add('Server='+ pServidor);

     try
        Connection.Connected := True;

        CarregaTabela(FDListProduto);
        CarregaTabela(FDProduto);


        Result.Result := True;
     except
       on e: exception do
       begin
            //Caso n�o consiga conectar resulta false e salva a msg de erro para enviar para a interface
            Result.Result := False;
            Result.Msg := 'N�o foi poss�vel estabelecer a conex�o.'+
                           sLineBreak + sLineBreak + e.Message;
       end;
     end;
end;

procedure TDMRegraNegocio.DataModuleCreate(Sender: TObject);
var
   vFMConfiguracao: TFMConfiguracao;
begin
     vFMConfiguracao := TFMConfiguracao.Create(Self);
     try
        vFMConfiguracao.ShowModal;
     finally
            FreeAndNil(vFMConfiguracao);
     end;
end;

procedure TDMRegraNegocio.FDProdutoAfterDelete(DataSet: TDataSet);
begin
     //Envia altera��o para o banco
     FDProduto.ApplyUpdates(-1);
end;

procedure TDMRegraNegocio.FDProdutoAfterPost(DataSet: TDataSet);
begin
     //Envia altera��o para o banco
     FDProduto.ApplyUpdates(-1);
end;

procedure TDMRegraNegocio.FDProdutoBeforePost(DataSet: TDataSet);
begin
     if Trim(FDProduto.FieldByName('descricao').AsString).IsEmpty then
     begin
          raise Exception.Create('O campo descri��o deve ser preenchido.');
     end;
end;

procedure TDMRegraNegocio.FDProdutoNewRecord(DataSet: TDataSet);
begin
     FDQueryAux.Close;
     FDQueryAux.SQL.Clear;
     FDQueryAux.SQL.Add('select max(codigo) as codigo ' +
                        ' from produto ');
     FDQueryAux.Open;

     FDProduto.FieldByName('codigo').AsInteger :=  FDQueryAux.FieldByName('codigo').AsInteger + 1;
end;

function TDMRegraNegocio.RemoverProduto: TRetorno;
begin
     try
        FDListProduto.Delete;

        CarregaTabela(FDListProduto);
     except
        on e: exception do
        begin
             Result.Result := False;
             Result.Msg := 'N�o foi poss�vel apagar o produto.'+
                            sLineBreak + sLineBreak + e.Message;
        end;
     end;
end;

end.
