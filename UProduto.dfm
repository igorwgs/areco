object FMProduto: TFMProduto
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Cadastro de produto'
  ClientHeight = 279
  ClientWidth = 566
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 0
    Width = 566
    Height = 45
    DataSource = DMRegraNegocio.DSProduto
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
    Align = alTop
    ConfirmDelete = False
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 45
    Width = 566
    Height = 193
    Align = alClient
    TabOrder = 1
    DesignSize = (
      566
      193)
    object Label1: TLabel
      Left = 26
      Top = 24
      Width = 40
      Height = 13
      Caption = 'C'#243'digo'
    end
    object Label2: TLabel
      Left = 26
      Top = 59
      Width = 56
      Height = 13
      Caption = 'Descri'#231#227'o'
    end
    object GridPanel1: TGridPanel
      Left = 14
      Top = 86
      Width = 547
      Height = 31
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      ColumnCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = Panel1
          Row = 0
        end
        item
          Column = 1
          Control = Panel2
          Row = 0
        end>
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 2
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 273
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          273
          31)
        object Label3: TLabel
          Left = 11
          Top = 7
          Width = 46
          Height = 13
          Caption = 'Unidade'
        end
        object DBEdit1: TDBEdit
          Left = 89
          Top = 4
          Width = 178
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          DataField = 'UNIDADE'
          DataSource = DMRegraNegocio.DSProduto
          TabOrder = 0
        end
      end
      object Panel2: TPanel
        Left = 273
        Top = 0
        Width = 274
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          274
          31)
        object Label4: TLabel
          Left = 10
          Top = 8
          Width = 32
          Height = 13
          Caption = 'Pre'#231'o'
        end
        object DBEdit2: TDBEdit
          Left = 64
          Top = 4
          Width = 196
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          DataField = 'VALOR'
          DataSource = DMRegraNegocio.DSProduto
          TabOrder = 0
        end
      end
    end
    object DBEdit3: TDBEdit
      Left = 104
      Top = 21
      Width = 121
      Height = 21
      TabStop = False
      Color = clBtnFace
      DataField = 'CODIGO'
      DataSource = DMRegraNegocio.DSProduto
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit4: TDBEdit
      Left = 104
      Top = 56
      Width = 442
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      DataField = 'DESCRICAO'
      DataSource = DMRegraNegocio.DSProduto
      TabOrder = 1
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 238
    Width = 566
    Height = 41
    Align = alBottom
    Color = 16728128
    ParentBackground = False
    TabOrder = 2
  end
end
