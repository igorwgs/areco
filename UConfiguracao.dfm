object FMConfiguracao: TFMConfiguracao
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Configura'#231#227'o de acesso'
  ClientHeight = 219
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 402
    Height = 46
    Align = alTop
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 392
    object Label5: TLabel
      Left = 14
      Top = 9
      Width = 132
      Height = 14
      Caption = 'Seja Bem-vindo(a)!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 24
      Top = 27
      Width = 283
      Height = 13
      Caption = 'Insira abaixo os dados para conex'#227'o ao servidor.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 46
    Width = 402
    Height = 132
    Align = alClient
    TabOrder = 1
    ExplicitLeft = -32
    ExplicitTop = 113
    ExplicitWidth = 392
    ExplicitHeight = 131
    DesignSize = (
      402
      132)
    object Label1: TLabel
      Left = 16
      Top = 72
      Width = 43
      Height = 13
      Caption = 'Usu'#225'rio'
    end
    object Label2: TLabel
      Left = 16
      Top = 99
      Width = 36
      Height = 13
      Caption = 'Senha'
    end
    object Label3: TLabel
      Left = 16
      Top = 45
      Width = 91
      Height = 13
      Caption = 'Banco de dados'
    end
    object Label4: TLabel
      Left = 16
      Top = 18
      Width = 49
      Height = 13
      Caption = 'Servidor'
    end
    object edBanco: TEdit
      Left = 112
      Top = 42
      Width = 274
      Height = 21
      Hint = 'Nome do banco de dados.'
      Anchors = [akLeft, akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = 'BD_IGOR'
    end
    object edUsuario: TEdit
      Left = 112
      Top = 69
      Width = 274
      Height = 21
      Hint = 'Usu'#225'rio utilizado para acessar o banco de dados'
      Anchors = [akLeft, akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Text = 'sa'
    end
    object edSenha: TEdit
      Left = 112
      Top = 96
      Width = 274
      Height = 21
      Hint = 'Senha utilizada para acessar o banco de dados. '
      Anchors = [akLeft, akTop, akRight]
      ParentShowHint = False
      PasswordChar = '*'
      ShowHint = True
      TabOrder = 3
      Text = 'masterkey'
    end
    object edServidor: TEdit
      Left = 112
      Top = 15
      Width = 274
      Height = 21
      Hint = 'Est'#226'ncia do servidor SQL Server.'
      Anchors = [akLeft, akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '.\SQLExpress'
      ExplicitWidth = 264
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 178
    Width = 402
    Height = 41
    Align = alBottom
    Color = 16728128
    ParentBackground = False
    TabOrder = 2
    ExplicitLeft = 8
    ExplicitTop = 168
    ExplicitWidth = 392
    object BitBtn1: TBitBtn
      Left = 296
      Top = 8
      Width = 87
      Height = 25
      Caption = '&Conectar'
      Default = True
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BitBtn1Click
    end
  end
end
