unit UProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Mask, Data.DB,
  UDMRegraNegocio;

type
  TFMProduto = class(TForm)
    DBNavigator1: TDBNavigator;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    GridPanel1: TGridPanel;
    Panel1: TPanel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Panel2: TPanel;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Panel3: TPanel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TFMProduto.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     if DMRegraNegocio.FDProduto.State in dsEditModes then
     begin
          if Application.MessageBox(PWideChar('Deseja salvar este registro?'),
                                    PWideChar('Salvar registro'),
                                    MB_YESNO + MB_ICONQUESTION) = mrYes then
          begin
               DMRegraNegocio.FDProduto.Post;
          end
          else
          begin
               DMRegraNegocio.FDProduto.Cancel;
          end;
     end;
end;

procedure TFMProduto.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if Key = VK_ESCAPE then
     begin
          Close;
     end;
end;

end.
