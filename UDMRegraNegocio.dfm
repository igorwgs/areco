object DMRegraNegocio: TDMRegraNegocio
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 267
  Width = 390
  object Connection: TFDConnection
    Params.Strings = (
      'Database=bd_igor'
      'User_Name=sa'
      'Password=masterkey'
      'Server=.\sqlexpress'
      'DriverID=MSSQL')
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    Connected = True
    LoginPrompt = False
    Left = 24
    Top = 88
  end
  object FDListProduto: TFDQuery
    Connection = Connection
    SQL.Strings = (
      'select * from produto')
    Left = 264
    Top = 40
  end
  object DSListProduto: TDataSource
    DataSet = FDListProduto
    Left = 264
    Top = 88
  end
  object FDProduto: TFDTable
    BeforePost = FDProdutoBeforePost
    AfterPost = FDProdutoAfterPost
    AfterDelete = FDProdutoAfterDelete
    OnNewRecord = FDProdutoNewRecord
    CachedUpdates = True
    IndexFieldNames = 'CODIGO'
    Connection = Connection
    UpdateOptions.AssignedValues = [uvUpdateMode, uvRefreshMode]
    UpdateOptions.RefreshMode = rmAll
    UpdateOptions.UpdateTableName = 'produto'
    UpdateOptions.KeyFields = 'codigo'
    TableName = 'produto'
    Left = 160
    Top = 48
  end
  object DSProduto: TDataSource
    DataSet = FDProduto
    Left = 160
    Top = 96
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 24
    Top = 24
  end
  object FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink
    Left = 32
    Top = 32
  end
  object FDQueryAux: TFDQuery
    Connection = Connection
    Left = 256
    Top = 176
  end
end
