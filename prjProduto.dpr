program prjProduto;

uses
  Vcl.Forms,
  MidasLib,
  UConfiguracao in 'UConfiguracao.pas' {FMConfiguracao},
  UDMRegraNegocio in 'UDMRegraNegocio.pas' {DMRegraNegocio: TDataModule},
  UListagem in 'UListagem.pas' {FMListProdutos},
  UProduto in 'UProduto.pas' {FMProduto};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDMRegraNegocio, DMRegraNegocio);

  if not DMRegraNegocio.Connection.Connected then
  begin
       Application.Terminate;
  end
  else
  begin
       Application.CreateForm(TFMListProdutos, FMListProdutos);
  end;

  Application.Run;
end.
